FROM gcc:latest

RUN useradd -ms /bin/bash ino

COPY client/client.c /app/
COPY database/database.c /app/
COPY dump/client_dump.c /app/

RUN gcc -o app/client app/client.c
RUN gcc -o app/database app/database.c
RUN gcc -o app/client_dump app/client_dump.c
RUN mkdir -p app/databases/databaseusers

USER ino