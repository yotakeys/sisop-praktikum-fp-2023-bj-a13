# Sisop Praktikum FP 2023 BJ-A13

Anggota :

1. Keyisa Raihan I. S. (5025211002)
2. M. Taslam Gustino (5025211011)
3. Adrian Karuna S. (5025211019)
4. Vito Febrian Ananta (5025211224)

### Autentikasi
Untuk melakukan autentikasi ada dua cara yaitu menggunakan sudo (user root) dan menggunakan username & password yang sudah dibuat oleh user root.
```c
bool isRoot()
{
    return (getuid() == 0);
}

bool authenticate(int argc, char const *argv[], int *sock)
{
    char buffer[1024] = {0}, response[20];
    int valread;
    if (!isRoot())
    {
        if (argc != 5 && (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0))
        {
            send(*sock, "error", strlen("error"), 0);
            printf("Authentication error!\n");
            printf("Syntax to start the client:\n");
            printf("./program -u [username] -p [password]\n");
            return false;
        }
        // Set first msg for authentication
        strcpy(buffer, argv[2]);
        strcat(buffer, " ");
        strcat(buffer, argv[4]);
        printf("%s\n", buffer);

        send(*sock, buffer, BUFFER_SIZE, 0);
    }
    else
    {
        if (argc == 1)
        {
            strcpy(buffer, "root");
            send(*sock, buffer, strlen(buffer), 0);
        }
    }
    recv(*sock, response, BUFFER_SIZE, 0);
    printf("res : %s\n", response);

    if (strcmp(response, "true") == 0)
    {
        isLogin = true;
    }

    memset(buffer, '\0', sizeof(buffer));
    return true;
}
```
Cara membedakan user yang sedang menggunakan client yaitu pada PID dan sudah dihandle oleh function isRoot. Apabila ingin login dengan user terdaftar cukup mengambil dari argument setelah -u dan -p yaitu argumen ke 2 dan 4 dan dikirimkan menggunakan socket. Kemudian set isLogin menjadi true untuk menghandle pergantian user nantinya.

```c
int createUser(char *request, char *response)
{
    char *file = "users.csv";
    char command[256];

    char user[50], pass[50];
    sscanf(request, "CREATE USER %s IDENTIFIED BY %s", user, pass);

    snprintf(command, sizeof(command), "echo '%s,%s' >> databases/databaseusers/%s", user, pass, file);

    // Execute the command using system
    int result = system(command);

    if (result == 0)
    {
        return EXIT_SUCCESS;
    }
    else
    {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

    else if (strstr(request, "CREATE USER"))
    {
        if (isRoot)
        {
            strcpy(response, "CREATE USER");
            printf("Masuk sini\n");
            if (strlen(request) > 12)
                createUser(request, response);
            create_log(user_client, request);
            addUserPermission(request, response);
        }
        strcpy(response, "ONLY ROOT CAN CREATE USER\n");
    }
```
Untuk menambahkan user yang hanya bisa dilakukan user root maka perlu handling dengan isRoot apabila ada perintah CREATE USER dan dipanggil create user. Fungsi createUser mencetak username dan password kedalam file bernama users.csv.

#### Output
![Screenshot_from_2023-06-24_23-18-41](/uploads/4db2aeffd4faa2b51fb5118849d837bf/Screenshot_from_2023-06-24_23-18-41.png)

### Autorisasi
Untuk melakukan autorisasi yang dapat mengakses database yang sudah dibuat dengan user apa aja baik root/user biasa.

```c
int useDatabase(char *request, char *response)
{
    char *delimiter = "USE";
    char *database_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        database_name = pos;

        database_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (database_name != NULL)
        {
            sscanf(pos, "%s", database_name);
        }
        else
        {
            strcpy(response, "nama database kosong\n");
            return EXIT_FAILURE;
        }
    }

    printf("database name: %s\n", database_name);

    // check if user has granted or not
    FILE *file = fopen("databases/databaseusers/permissions.csv", "r");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka permissions.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file);

    static int useState;
    int grantStatus;

    while (fgets(line, sizeof(line), file) != NULL)
    {
        char database[MAX_LENGTH];
        char user[MAX_LENGTH];
        sscanf(line, "%d,%[^,],%s", &grantStatus, database, user);

        if (strcmp(database_name, database) == 0 && strcmp(userClient, user) == 0)
        {
            useState = grantStatus;
            break;
        }
    }

    fclose(file);
    if (useState == 1 || isRoot)
    {
        int database_state_length = 0;
        while (database_state[database_state_length].database != NULL)
        {
            if (strcmp(database_state[database_state_length].database, database_name) == 0)
            {
                databaseUsed = malloc(strlen(database_name) + 1);
                strcpy(databaseUsed, database_name);
                database_state[database_state_length].state = 1;
                break;
            }
            database_state_length++;
        }

        return EXIT_SUCCESS;
    }
    else
    {
        strcpy(response, "User has no access to use this database\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

    else if (strstr(request, "USE"))
    {
        strcpy(response, "USE");
        useDatabase(request, response);
        create_log(user_client, request);
    }
```
Pada saat menggunakan perintah USE maka dapat melakukan DDL ataupun DML pada database yang berkaitan. Dilakukan pengecekan apakah ada folder database dan apakah user dapat akses terhadap database tersebut. Sedangkan untuk user root langsung diberi akses untuk database manapun.

```c
int grantPermission(char *request, char *response)
{

    DIR *dir;
    struct dirent *entry;

    char *substr1 = "PERMISSION";
    char *substr2 = "INTO";

    // mencari posisi substring "PERMISSION"
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax PERMISSION tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax INTO tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    // Get Database Name
    int length = pos2 - pos1 - 1;
    char database_name[length];
    strncpy(database_name, pos1, length);
    database_name[length] = '\0';

    printf("Database Name: %s\n", database_name);

    // Get User Name
    char *username = NULL;

    char *pos = strstr(request, substr2);
    if (pos != NULL)
    {
        pos += strlen(substr2) + 1;
        username = pos;

        username = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (username != NULL)
        {
            sscanf(pos, "%s", username);
        }
    }

    if (username != NULL)
    {
        printf("Username: %s\n", username);
    }

    FILE *file = fopen("databases/databaseusers/permissions.csv", "r");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka permissions.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    int databaseFound = 0;
    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file);

    while (fgets(line, sizeof(line), file) != NULL)
    {
        char *token = strtok(line, ",");
        if (token != NULL && strcmp(token, "0") == 0)
        {
            token = strtok(NULL, ",");
            printf("Token: %s\n", token);
            if (token != NULL && strcmp(token, database_name) == 0)
            {
                token = strtok(NULL, ",");
                printf("Token: %s\n", token);
                printf("Username: %s\n", username);
                // username = strcat(username, "\n");
                if (token != NULL && strcmp(token, username) == 0 || strcspn(username, "\n") == strlen(username))
                {
                    databaseFound = 1;
                    break;
                }
            }
        }
    }

    fclose(file);

    if (databaseFound)
    {
        printf("Database found\n");
        char command[MAX_LENGTH + 200];
        sprintf(command, "awk -F',' -v db='%s' -v usr='%s' 'BEGIN {OFS=\",\"} NR>1 && $2==db && $3==usr {sub(/0/, \"1\", $1)} { print }' databases/databaseusers/permissions.csv > databases/databaseusers/temp.csv && mv databases/databaseusers/temp.csv databases/databaseusers/permissions.csv", database_name, username);
        int status = system(command);
        if (status == 0)
        {
            printf("Permission granted\n");
            strcpy(response, "Permission granted\n");
        }
        else
        {
            printf("Permission not granted\n");
            strcpy(response, "Permission not granted\n");
        }
    }
    else
    {
        printf("Database not found\n");
        strcpy(response, "Database tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    return 1;
}

    if (strstr(request, "GRANT PERMISSION") && isRoot)
    {
        strcpy(response, "GRANT PERMISSION");
        grantPermission(request, response);
        create_log(user_client, request);
    }
```
Untuk grand permission hanya dapat dilakukan oleh user root. Didalamnya melakukan pengecekan syntax, database, username. Apabila ada semuanya maka database ditemukan dan merubah permission menjadi 1 didalam permissions.csv.

#### Output
![Screenshot_from_2023-06-24_23-18-51](/uploads/fa3b31a403c5efa52367cf6678fbb1cd/Screenshot_from_2023-06-24_23-18-51.png)

### DDL

#### CREATE DATABASE
Untuk data definition language yang pertama yaitu create database dimana semua user dapat melakukan create database
```c
int createDatabase(char *request, char *response)
{
    char *delimiter = "DATABASE";
    char *database_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        database_name = pos;

        database_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (database_name != NULL)
        {
            sscanf(pos, "%s", database_name);
            char command[MAX_LENGTH + 100];
            sprintf(command, "mkdir databases/%s", database_name);
            system(command);
        }
        else
        {
            strcpy(response, "nama database kosong\n");
            return EXIT_FAILURE;
        }
    }

    FILE *file = fopen("databases/databaseusers/users.csv", "r");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka users.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file);

    char command[BUFFER_SIZE];

    fprintf(file, "\n");

    while (fgets(line, sizeof(line), file) != NULL)
    {
        char *username = strtok(line, ",");
        if (username != NULL)
        {
            if (strcmp(username, userClient) == 0)
            {
                printf("username: %s\n", username);
                sprintf(command, "echo '1,%s,%s' >> 'databases/databaseusers/permissions.csv'", database_name, username);
                system(command);
            }
            else
            {
                printf("username: %s\n", username);
                sprintf(command, "echo '0,%s,%s' >> 'databases/databaseusers/permissions.csv'", database_name, username);
                system(command);
            }
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}
    else if (strstr(request, "CREATE DATABASE"))
    {
        strcpy(response, "CREATE DATABASE");
        createDatabase(request, response);
        create_log(user_client, request);
    }
```
Didalam createDatabase function melakukan pengecekan syntax dan pengecekan permission user. Apabila semuanya aman maka dibuat folder dengan nama database yang sesuai dan melakukan penulisan kedalam permission. Untuk user yang membuat langsung diberi permission apabila root yang membuat kedua user tidak memiliki permission kedalam database tersebut.

#### Output
![Screenshot_from_2023-06-24_23-21-50](/uploads/e8a00b9101b746bada56b3af736b0dec/Screenshot_from_2023-06-24_23-21-50.png)

#### CREATE TABLE
DDL selanjutnya yaitu create table
```c
int createTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[100];
    sscanf(request, "CREATE TABLE %s", tableName);

    // Membuat nama file dengan format <tableName>.csv
    // char fileName[100];
    char fileName[MAX_LENGTH + 100];
    char command[MAX_LENGTH + 100];
    sprintf(command, "touch databases/%s/%s.csv", databaseUsed, tableName);
    printf("database used: %s\n", databaseUsed);
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);
    system(command);
    // sprintf(fileName, "%s.csv", tableName);

    // Membuka file untuk ditulis
    FILE *file = fopen(fileName, "w");
    if (file == NULL)
    {
        printf("Gagal membuat file CSV.\n");
        return EXIT_FAILURE;
    }

    // Menulis baris pertama dengan tipe data kolom
    char *columnStart = strchr(request, '(') + 1;
    char *columnEnd = strchr(request, ')');
    *columnEnd = '\0'; // Mengganti ')' dengan null terminator

    // Menghapus spasi setelah koma
    char *currentChar = columnStart;
    while (currentChar != columnEnd)
    {
        if (*currentChar == ',' && *(currentChar + 1) == ' ')
        {
            memmove(currentChar + 1, currentChar + 2, strlen(currentChar + 1) + 1);
        }
        currentChar++;
    }

    char *columnName = strtok(columnStart, ",");
    while (columnName != NULL)
    {
        fprintf(file, "%s", columnName);

        columnName = strtok(NULL, ",");
        if (columnName != NULL)
        {
            fprintf(file, ",");
        }
    }
    fprintf(file, "\n");

    // Menutup file
    fclose(file);

    printf("File CSV '%s' telah berhasil dibuat.\n", fileName);

    return EXIT_SUCCESS;
}
    else if (strstr(request, "CREATE TABLE"))
    {
        strcpy(response, "CREATE TABLE");
        createTable(request, response);
        create_log(user_client, request);
    }
```
Pertama dilakukan pengecekan flag database yang sedang digunakan. Kemudian membuat file dengan nama table didalam folder database yang sedang digunakan dan menulis nama kolom (attribut) yang sesuai dengan tipe datanya didalam file yang baru saja dibuat.

#### Output
![Screenshot_from_2023-06-24_23-24-40](/uploads/b65f2540b57db4fe90363898e3b4d207/Screenshot_from_2023-06-24_23-24-40.png)

#### DROP DATABASE
```c
int dropDatabase(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }
    char *delimiter = "DATABASE";
    char *database_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        database_name = pos;

        database_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (database_name != NULL)
        {
            sscanf(pos, "%s", database_name);
        }
        else
        {
            strcpy(response, "nama database kosong\n");
            return EXIT_FAILURE;
        }
    }

    if (strcmp(database_name, databaseUsed) != 0)
    {
        strcpy(response, "Database sedang tidak digunakan\n");
        return EXIT_FAILURE;
    }
    else
    {
        char command[MAX_LENGTH + 100];
        sprintf(command, "rm -rf databases/%s", database_name);
        system(command);
    }

    return EXIT_SUCCESS;
}
    
    else if (strstr(request, "DROP DATABASE"))
    {
        strcpy(response, "DROP DATABASE");
        dropDatabase(request, response);
        create_log(user_client, request);
    }
```
Dilakukan pengecekan syntax dan database yang sedang digunakan. Apabila ada maka langsung dilakukan delete pada folder database

#### DROP TABLE
```c
int dropTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }
    char *delimiter = "TABLE";
    char *table_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        table_name = pos;

        table_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (table_name != NULL)
        {
            sscanf(pos, "%s", table_name);
        }
        else
        {
            strcpy(response, "nama table kosong\n");
            return EXIT_FAILURE;
        }
    }

    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);
    remove(file_name);

    return EXIT_SUCCESS;
}

    else if (strstr(request, "DROP TABLE"))
    {
        strcpy(response, "DROP TABLE");
        dropTable(request, response);
        create_log(user_client, request);
    }
```
Dilakukan pengecekan syntax dan database yang digunakan. apabila ada maka dilakukan delete pada file table yang ada

#### DROP COLUMN
```c
int dropColumn(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *substr1 = "COLUMN";
    char *substr2 = "FROM";

    // mencari posisi substring "PERMISSION"
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax COLUMN tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax FROM tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    // Get Database Name
    int length = pos2 - pos1 - 1;
    char column_name[length];
    strncpy(column_name, pos1, length);
    column_name[length] = '\0';

    printf("Column Name: %s\n", column_name);

    char *delimiter = "FROM";
    char *table_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        table_name = pos;

        table_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (table_name != NULL)
        {
            sscanf(pos, "%s", table_name);
        }
        else
        {
            strcpy(response, "nama table kosong\n");
            return EXIT_FAILURE;
        }
    }

    printf("Table Name: %s\n", table_name);

    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    char command[MAX_LENGTH + 1000];
    sprintf(command, "sed -i 's/^%s[^,]*,//' %s", column_name, file_name);
    system(command);

    return EXIT_SUCCESS;
}

    else if (strstr(request, "DROP COLUMN"))
    {
        strcpy(response, "DROP COLUMN");
        dropColumn(request, response);
        create_log(user_client, request);
    }
```
Dilakukan pengecekan syntax dan database yang digunakan. Kemudian mencari nama table yang akan dihapus kolomnya. Apabila ada maka dilakukan delete pada nama kolom yang ada di file table yang sudah ditentukan dari syntaxnya

### Logging
Untuk melakukan logging ke file (database_log.log di ../database/) dengan format timestamp(yyyy-mm-dd hh:mm:ss):username:command pada setiap command yang dipakai (Jika yang eksekusi root, maka username root), berikut adalah caranya.

Pertama-tama kita perlu mendapatkan timestampnya terlebih dahulu, dengan cara megambil valuenya melalui time() dengan bantuan library time.h, lalu mengubah format dari epoch UNIX menjadi format yang diminta. Berikut adalah kode program fungsinya:
```c
char *get_current_time()
{
    char *current_time = (char *)malloc(100 * sizeof(char));
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(current_time, 100, "%Y-%m-%d %H:%M:%S", tm);
    return current_time;
}
```

Setelah itu, kita dapat membuat fungsi untuk membuat log ke file bernama database_log.log yang berisi format yang diminta seperti pada soal, sehingga kita memperlukan paramater yang dibutuhkan selanjutnya yaitu user dan command. Berikut adalah kode programnya:
```c
void create_log(char *username, char *command)
{
    char *current_time = get_current_time();

    FILE *file;
    file = fopen("database_log.log", "a");
    if (file == NULL)
    {
        printf("Failed to open the log file.\n");
        free(current_time); // Free the dynamically allocated memory before returning
        return;
    }
    fprintf(file, "%s:%s:%s\n", current_time, username, command);
    fclose(file);
    free(current_time); // Free the dynamically allocated memory
}
```

Fungsi tersebut akan dipanggil pada fungsi execute() di program database.c yaitu dimana command-command akan dipakai. Berikut adalah contoh outputnya:
![output_logging](/uploads/782e4492df50b6db2c2b8464a3a37984/output_logging.png)


### DATA MANIPULATION LANGUAGE  


#### INSERT INTO

```c
int insertIntoTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *substr1 = "INTO";
    char *substr2 = "VALUES";

    // Find the position of "INTO" substring
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax INTO not found\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    // Find the position of "VALUES" substring
    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax VALUES not found\n");
        return EXIT_FAILURE;
    }

    // Get the table name
    int length = pos2 - pos1 - 1;
    char table_name[length];
    strncpy(table_name, pos1, length);
    table_name[length] = '\0';

    printf("Table Name: %s\n", table_name);

    // Get the values to be inserted
    pos2 += strlen(substr2) + 1;
    char values[MAX_LENGTH];
    strncpy(values, pos2, MAX_LENGTH);

    // Remove leading and trailing whitespace characters
    char trimmed_values[MAX_LENGTH];
    int i = 0, j = 0;
    while (values[i] != '\0')
    {
        if (values[i] != ' ' && values[i] != '(' && values[i] != ')')
        {
            trimmed_values[j++] = values[i];
        }
        i++;
    }
    trimmed_values[j] = '\0';

    printf("Values: %s\n", trimmed_values);

    // Create the file name
    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    // Open the file in append mode
    FILE *file = fopen(file_name, "a");
    if (file == NULL)
    {
        strcpy(response, "Failed to open CSV file\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    // Write the values to a new line in the CSV file
    fprintf(file, "%s\n", trimmed_values);

    // Close the file
    fclose(file);

    return EXIT_SUCCESS;
}
```


- untuk melakukan insert maka kita mengirimkan query insert into, yang akan dicek pada code berikut : 

```c 
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *substr1 = "INTO";
    char *substr2 = "VALUES";

    // Find the position of "INTO" substring
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax INTO not found\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    // Find the position of "VALUES" substring
    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax VALUES not found\n");
        return EXIT_FAILURE;
    }
```

- lalu pada bagian dibawah ini, kita akan mengambil value yang akan di insert, lalu menghapus tanda "()" yang ada pada query insert, dan kemudian akan di append dengan 
```c   
// Get the values to be inserted
    pos2 += strlen(substr2) + 1;
    char values[MAX_LENGTH];
    strncpy(values, pos2, MAX_LENGTH);

    // Remove leading and trailing whitespace characters
    char trimmed_values[MAX_LENGTH];
    int i = 0, j = 0;
    while (values[i] != '\0')
    {
        if (values[i] != ' ' && values[i] != '(' && values[i] != ')')
        {
            trimmed_values[j++] = values[i];
        }
        i++;
    }
    trimmed_values[j] = '\0';

    printf("Values: %s\n", trimmed_values);

    // Create the file name
    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    // Open the file in append mode
    FILE *file = fopen(file_name, "a");
    if (file == NULL)
    {
        strcpy(response, "Failed to open CSV file\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    // Write the values to a new line in the CSV file
    fprintf(file, "%s\n", trimmed_values);

    // Close the file
    fclose(file);
```
![image](/uploads/a554bfe2e9ec04299d530e13b5e306bd/image.png)

#### SELECT 

##### SELECT ALL 

```c 
int selectAllFromTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[100];
    sscanf(request, "SELECT * FROM %s", tableName);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    // Open the file for reading
    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", tableName);
        return EXIT_FAILURE;
    }

    // Read and print each line in the file
    char line[MAX_LENGTH];
    strcat(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        strcat(response, line);
    }

    // Close the file
    fclose(file);

    return EXIT_SUCCESS;
}
```

- pada select all, pertama kita cek apakah sudah menggunakan (USE) database 
- lalu kita ambil nama tabel dan nama file, dan kita buka file pada mode read, lalu dengan while kita concat/append ke sebuah file yang nanti akan di print ke layar

```c
    char tableName[100];
    sscanf(request, "SELECT * FROM %s", tableName);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    // Open the file for reading
    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", tableName);
        return EXIT_FAILURE;
    }

    // Read and print each line in the file
    char line[MAX_LENGTH];
    strcat(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        strcat(response, line);
    }

    // Close the file
    fclose(file);
```

![image](/uploads/c47f6d32c701b8c9ecde22c7f1fd3071/image.png)

##### SELECT ALL + WHERE

```c
int sellect_all_from_table_where(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    char condition[MAX_LENGTH];
    sscanf(request, "SELECT * FROM %s WHERE %s = %[^\n]", tableName, columnName, condition);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column names to the response
    strcpy(response, line);
    strcpy(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    strcat(response, line); // Append the original line to the response
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}
```

- kurang lebih metodenya sama dengan select all tapi, disini kita perlu hanya mengambil data yang diperlukan sesuai dengan where yang diberikan jadi disini terdapat pengecekan : 
    - column exist?
    - extract column value yang sesuai dengan where 

- disini dicek apakah column exist 
```c
   // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }
```

- lalu disini dengan while kita cek semua line/row apabila ada yang sesuai dengan where maka akan di concat/append ke result kemduian akan di print

```c

    // Copy the column names to the response
    strcpy(response, line);
    strcpy(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    strcat(response, line); // Append the original line to the response
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }
```

![image](/uploads/2224dc986e033779e265eead3532f3aa/image.png)


##### SELECT COLUMN 

```c

int select_column_from_table(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    sscanf(request, "SELECT %s FROM %s", columnName, tableName);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Append the column value to the response
                strcat(response, columnValue);
                strcat(response, "\n");
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}
```
- kurang lebih mirip dengan sebelumnya, disini ada pengecekan apakah column nya exist. Lalu kita copy hanya nama column yang diinginkan pada response (supaya saat di print hanya ada column yang dicari saja tidak semua column dicantumkan seperti di select all)

- lalu kita create temporary file dan sebuah while untuk iterate semua insert/row yang ada pada table, lalu dengan while kita masukkan hanya column yang diinginkan pada file temporary tersebut

- dapat dilihat pada potongan code dibawah ini

```c
   // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Append the column value to the response
                strcat(response, columnValue);
                strcat(response, "\n");
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
```

![image](/uploads/2722a7993032c661ad66b0890855714a/image.png)


##### SELECT COLUMN + WHERE

```c
int select_column_from_table_where(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    char condition[MAX_LENGTH];
    sscanf(request, "SELECT %s FROM %s WHERE %s", columnName, tableName, condition);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Check if the condition is met
                if (strcmp(columnValue, condition) == 0)
                {
                    // Append the column value to the response
                    strcat(response, columnValue);
                    strcat(response, "\n");
                }
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}
```

- disini modifikasi dari sebelumnya adalah pada kondisi where nya pada 

```c
while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Check if the condition is met
                if (strcmp(columnValue, condition) == 0)
                {
                    // Append the column value to the response
                    strcat(response, columnValue);
                    strcat(response, "\n");
                }
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
```

- dimana apabila columnvalue memenuhi condition, maka baru akan di concat



##### DELETE ALL
```c

int deleteFromTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *delimiter = "FROM";
    char *table_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        table_name = pos;

        table_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (table_name != NULL)
        {
            sscanf(pos, "%s", table_name);
        }
        else
        {
            strcpy(response, "Table name is empty\n");
            return EXIT_FAILURE;
        }
    }

    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    char command[MAX_LENGTH + 1000];
    sprintf(command, "sed -i '2,$d' %s", file_name);
    system(command);

    return EXIT_SUCCESS;
}

```
- pada code ini delete semua row pada table dilakukan dengan menggunakan terminal command sed, yang dieksekusi dengan system()
- dimana sed ini menghapus semua isi dari file .csv kecuali row pertama 

```c
    char command[MAX_LENGTH + 1000];
    sprintf(command, "sed -i '2,$d' %s", file_name);
    system(command);
```

![image](/uploads/3cb2955be604bc88c10aa8e4d7d33e7a/image.png)

##### DELETE + WHERE   
```c
int delete_from_table_where(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    char condition[MAX_LENGTH];
    sscanf(request, "DELETE FROM %s WHERE %s = %[^\n]", tableName, columnName, condition);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char tempFileName[MAX_LENGTH + 100];
    sprintf(tempFileName, "databases/%s/temp.csv", databaseUsed);

    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL)
    {
        printf("Failed to create temporary CSV file.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)
    fputs(line, tempFile);
    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        fclose(tempFile);
        return EXIT_FAILURE;
    }

    // Copy the column names to the response
    strcpy(response, line);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        int matchFound = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    matchFound = 1;
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }

        // If the condition is not met, write the line to the temporary file
        if (!matchFound)
            fputs(line, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove(fileName);
    rename(tempFileName, fileName);

    return EXIT_SUCCESS;
}
```
- untuk delete where ini, kurang lebih caranya mirip dengan select + where.
- kita menggunakan while dan tempfile, didalam while kita cek apabila ada row yang tidak sesuai dengan kondisi where, maka dimasukkan kedalam tempfile
- kemudian file lama dihapus, dan digantikan oleh tempfile yang berisikan file tanpa konten yang ingin dihapus
- hal ini dilakukan pada :
```c
while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    matchFound = 1;
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }

        // If the condition is not met, write the line to the temporary file
        if (!matchFound)
            fputs(line, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove(fileName);
    rename(tempFileName, fileName);

```

![image](/uploads/f45273bca14ced55afe307914a84cb8e/image.png)


##### UPDATE DATA
```c

int updateData(const char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    // Extract table name, set column, set new value, where column, and where value from the request
    char table[100];
    char setColumn[100];
    char setNewValue[100];
    char whereColumn[100];
    char whereValue[100];

    if (sscanf(request, "UPDATE %s SET %s = %s WHERE %s = %s", table, setColumn, setNewValue, whereColumn, whereValue) != 5)
    {
        strcpy(response, "Invalid UPDATE command\n");
        return EXIT_FAILURE;
    }

    // Construct the file path
    char filePath[200];
    sprintf(filePath, "databases/%s/%s.csv", databaseUsed, table);

    // Open the CSV file for reading
    FILE *file = fopen(filePath, "r");
    if (file == NULL)
    {
        sprintf(response, "Failed to open the file '%s'\n", filePath);
        return EXIT_FAILURE;
    }

    // Create a temporary file for writing the updated data
    FILE *tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL)
    {
        fclose(file);
        sprintf(response, "Failed to create the temporary file\n");
        return EXIT_FAILURE;
    }

    // Read and process the header line
    char headerLine[1024];
    fgets(headerLine, sizeof(headerLine), file);
    fputs(headerLine, tempFile);

    // Read and process each data line
    char dataLine[1024];
    int updated = 0;

    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (columnIndex == 0 && strcmp(currentColumn, whereValue) == 0 && strcmp(setColumn, whereColumn) == 0)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (updated)
    {
        // Replace the original file with the temporary file
        if (remove(filePath) != 0)
        {
            sprintf(response, "Failed to remove the original file\n");
            return EXIT_FAILURE;
        }

        if (rename("temp.csv", filePath) != 0)
        {
            sprintf(response, "Failed to rename the temporary file\n");
            return EXIT_FAILURE;
        }

        sprintf(response, "Data updated successfully.\n");
    }
    else
    {
        // Remove the temporary file if no updates were made
        remove("temp.csv");
        sprintf(response, "No matching records found for the given conditions.\n");
    }

    return EXIT_SUCCESS;
}
```
- untuk update juga ada kemiripan pada while nya, dimana kita hanya mengecek apabila kondisi row nya sesuai dengan kondisi, maka kita akan melakukan update data nya 

```c 
    // Read and process the header line
    char headerLine[1024];
    fgets(headerLine, sizeof(headerLine), file);
    fputs(headerLine, tempFile);

    // Read and process each data line
    char dataLine[1024];
    int updated = 0;

    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (columnIndex == 0 && strcmp(currentColumn, whereValue) == 0 && strcmp(setColumn, whereColumn) == 0)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }
```

![image](/uploads/08eca69ce4bea758a775f954faa8b1fc/image.png)

### Dump
Untuk mengbackup data bisa dilakukan dengan 2 cara, yaitu melalui `client.c` ataupun melalui `client_dump.c`. Tetapi sebelum menjalankan kedua program tersebut, harus dipastikan bahwa database sserver sudah dijalan kan terlebih dahulu agar tidak menyebabkan connection failed.
- client_dump.c 
```c
// Compile
gcc -o client_dump client_dump.c -lpthread

// Syntax
./client_dump -u {user} -p {pass} {db_name} TO {name_file}

// Contoh
./client_dump -u user1 -p 12345 test1 TO test1.backup

```
- client.c
```c
// Syntax
./client -u {user} -p {pass} -d {db_name} TO {nama_file}

//Contoh
./client -u user1 -p 12345 -d test1 TO test1.backup
```
Nantinya setiap program akan memanggil `client_dump.c`, yang akan memanggil fungsi `BACKUP DATABASE` dari database server.
```c
 char buffer[BUFFER_SIZE];
    
    strcpy(buffer, "BACKUP DATABASE ");
    strcat(buffer, argv[5]);

    strcpy(name_file, argv[7]);

    send(sock, buffer, strlen(buffer), 0);

    pthread_t dataHandler;
    pthread_create(&dataHandler, NULL, &messageHandling, &sock);


    memset(buffer, 0, sizeof(buffer));

    // Join thread
    pthread_join(dataHandler, NULL);

    close(sock);

```

didalam `database.c` didefinisikan fungsi `backupData()`. fungsi ini akan membaca dan mentraverse directory database, sesuai database yang ingin dibackup. setiap table akan dibaca isinya dan diubah menjadi sebuah perintah database.
```c
int backupData(char *request, char *response){

    char namaDB[500];

    sscanf(request, "BACKUP DATABASE %s", namaDB);

    char directoryDB[500];
    sprintf(directoryDB, "databases/%s", namaDB);
    DIR* dir = opendir(directoryDB);

    if (dir) {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) {
            entry->d_name[strlen(entry->d_name) -4] = '\0';

            // Exclude current directory (".") and parent directory ("..")
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                sprintf(response, "DROP TABLE %s", entry->d_name);
                send(new_socket, response, BUFFER_SIZE, 0);

                char dirfile[200];
                sprintf(dirfile, "%s/%s.csv", directoryDB, entry->d_name);
                FILE* file = fopen(dirfile, "r");
                if (file == NULL) {
                    printf("Failed to open the file.\n");
                    return 1;
                }

                char line[BUFFER_SIZE];
                int idx = 1;
                while (fgets(line, sizeof(line), file) != NULL) {
                    // Process the line
                    if(idx == 1){
                        line[strlen(line) -1] = '\0';
                        sprintf(response, "CREATE TABLE (%s);", line);
                        send(new_socket, response, BUFFER_SIZE, 0);
                    }else{
                        line[strlen(line) -1] = '\0';
                        sprintf(response, "INSERT INTO %s VALUES (%s);", entry->d_name, line);
                        send(new_socket, response, BUFFER_SIZE, 0);
                    }

                    idx++;
                }

                fclose(file);
                send(new_socket, "\n", BUFFER_SIZE, 0);
            }

        }
        closedir(dir);

        sprintf(response, "//DONE BACKUP");
    } else {
        sprintf(response, "Database not exist\n");
        return EXIT_FAILURE;
    }

}
```
Setiap kali membuat perintah database, perintah tersebut akan dikirimkan ke socket yang terhubung dengan `client_dump.c`. Hasil yang didapat dari database oleh client_dump, akan dimasukkan kedalam file yang ingin dituju sebagai backup
```c
void *messageHandling(void *arg)
{
    char buffer[1024];

    memset(buffer, 0, sizeof(buffer));
    while (read(*(int *)arg, buffer, 1024) != 0)
    {
        if (strlen(buffer)){
            char cmd[100];
            sprintf(cmd, "echo \"%s\" >> %s", buffer, name_file);
            system(cmd);
        }

        memset(buffer, 0, sizeof(buffer));
    }
}
```

##### Output: 
![image](/uploads/ecb919fbaf1f3a400133d963658ccc40/image.png)

### CRON
Untuk CRON, setiap jam `0 * * * *`, maka kita akan cd ke directory dump, lalu menjalankan program `client_dump`, hasil dari backup akan di zip lalu file asli akan di remove. terakhir file log akan di buang, lalu dibuat file log yang baru.

```sh
0 * * * * cd /home/key/sisop-praktikum-fp-2023-bj-a13/dump && ./client_dump -u user1 -p 12345 test1 to test1.backup && zip test1.zip test1.backup && rm test1.backup && cd ../database && rm database_log.log && touch database_log.log
```

### Containerization
```
FROM gcc:latest

RUN useradd -ms /bin/bash ino

COPY client/client.c /app/
COPY database/database.c /app/
COPY dump/client_dump.c /app/

RUN gcc -o app/client app/client.c
RUN gcc -o app/database app/database.c
RUN gcc -o app/client_dump app/client_dump.c
RUN mkdir -p app/databases/databaseusers

USER ino
```
Didalam dockerfile berisi kode diatas dimana container yang dibuat dari gcc dan menambahkan user 'ino' didalam container untuk membedakan user nantinya. Kemudian melakukan copy file yang nantinya akan dirun didalam container perlu di compile terlebih dahulu menggunakan gcc. Untuk tambahan database yang dibuat nantinya akan dimasukkan kedalam folder app/databases/databaseusers didalam container.

```
version: "3"
services:
  Sukolilo:
    build: ./Sukolilo
    image: fsisop
    ports:
      - 8050-8054:8080
    deploy:
      replicas: 5
  Keputih:
    build: ./Keputih
    image: fsisop
    ports:
      - 8055-8059:8080
    deploy:
      replicas: 5
  Gebang:
    build: ./Gebang
    image: fsisop
    ports:
      - 8060-8064:8080
    deploy:
      replicas: 5
  Mulyos:
    build: ./Mulyos
    image: fsisop
    ports:
      - 8065-8069:8080
    deploy:
      replicas: 5
  Semolowaru:
    build: ./Semolowaru
    image: fsisop
    ports:
      - 8070-8074:8080
    deploy:
      replicas: 5

```
Untuk melakukan docker compose diperlukan folder sesuai soal dan didalamnya terdiri dari file dan folder yang akan dicopy kedalam container. untuk port harus dibedakan dalam setiap replicas yang dibuat.

#### Output
![Screenshot_from_2023-06-24_23-17-30](/uploads/cc729318c6f829fb1ac219714f9a1d9f/Screenshot_from_2023-06-24_23-17-30.png)
![Screenshot_from_2023-06-24_22-22-09](/uploads/ee8c1518ca189b90e618598f50690b91/Screenshot_from_2023-06-24_22-22-09.png)
