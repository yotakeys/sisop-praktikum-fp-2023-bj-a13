#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#include <time.h>
#include <ctype.h>

#define PORT 8080
#define BUFFER_SIZE 1024
#define MAX_LENGTH 100


int new_socket;
char userClient[50], passClient[50];
char *databaseUsed;
bool isRoot = false;

char *get_current_time()
{
    char *current_time = (char *)malloc(100 * sizeof(char));
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(current_time, 100, "%Y-%m-%d %H:%M:%S", tm);
    return current_time;
}

void create_log(char *username, char *command)
{
    char *current_time = get_current_time();

    FILE *file;
    file = fopen("database_log.log", "a");
    if (file == NULL)
    {
        printf("Failed to open the log file.\n");
        free(current_time); // Free the dynamically allocated memory before returning
        return;
    }
    fprintf(file, "%s:%s:%s\n", current_time, username, command);
    fclose(file);
    free(current_time); // Free the dynamically allocated memory
}

struct DatabaseState
{
    char *database;
    int state;
};

struct DatabaseState database_state[100] = {NULL, 0};

int createUser(char *request, char *response)
{
    char *file = "users.csv";
    char command[256];

    char user[50], pass[50];
    sscanf(request, "CREATE USER %s IDENTIFIED BY %s", user, pass);

    snprintf(command, sizeof(command), "echo '%s,%s' >> databases/databaseusers/%s", user, pass, file);

    // Execute the command using system
    int result = system(command);

    if (result == 0)
    {
        return EXIT_SUCCESS;
    }
    else
    {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void generateDatabaseStateByUserClient()
{
    // get all database
    DIR *dir;
    struct dirent *entry;
    dir = opendir("databases");
    if (dir == NULL)
    {
        perror("opendir");
        exit(EXIT_FAILURE);
    }
    int database_state_length = 0;
    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_DIR)
        {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
            {
                // printf("%s\n", entry->d_name);
                database_state[database_state_length].database = malloc(strlen(entry->d_name) + 1);
                strcpy(database_state[database_state_length].database, entry->d_name);
                database_state[database_state_length].state = 0;
                // printf("database name: %s\n", database_state[database_state_length].database);
                // printf("database state: %d\n", database_state[database_state_length].state);
                database_state_length++;
            }
        }
    }

    closedir(dir);
}

int useDatabase(char *request, char *response)
{
    char *delimiter = "USE";
    char *database_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        database_name = pos;

        database_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (database_name != NULL)
        {
            sscanf(pos, "%s", database_name);
        }
        else
        {
            strcpy(response, "nama database kosong\n");
            return EXIT_FAILURE;
        }
    }

    printf("database name: %s\n", database_name);

    // check if user has granted or not
    FILE *file = fopen("databases/databaseusers/permissions.csv", "r");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka permissions.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file);

    static int useState;
    int grantStatus;

    while (fgets(line, sizeof(line), file) != NULL)
    {
        // int grantStatus;
        char database[MAX_LENGTH];
        char user[MAX_LENGTH];
        sscanf(line, "%d,%[^,],%s", &grantStatus, database, user);
        // printf("grantStatus: %d\n", grantStatus);
        // printf("database: %s\n", database);
        // printf("user: %s\n", user);

        // delete \n in user
        // user[strlen(user) - 1] = '\0'; // iso salah on aneh

        if (strcmp(database_name, database) == 0 && strcmp(userClient, user) == 0)
        {
            useState = grantStatus;
            break;
        }
    }

    fclose(file);
    if (useState == 1 || isRoot)
    {
        // strcpy(response, "User has access to use this database\n");
        int database_state_length = 0;
        while (database_state[database_state_length].database != NULL)
        {
            // printf("database state name: %s\n", database_state[database_state_length].database);
            // printf("database name check: %s\n", database_name);
            if (strcmp(database_state[database_state_length].database, database_name) == 0)
            {
                // printf("database_state_length: %d\n", database_state_length);
                databaseUsed = malloc(strlen(database_name) + 1);
                strcpy(databaseUsed, database_name);
                database_state[database_state_length].state = 1;
                // printf("database state: %d\n", database_state[database_state_length].state);
                break;
            }
            database_state_length++;
        }

        return EXIT_SUCCESS;
    }
    else
    {
        strcpy(response, "User has no access to use this database\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int createDatabase(char *request, char *response)
{
    char *delimiter = "DATABASE";
    char *database_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        database_name = pos;

        database_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (database_name != NULL)
        {
            sscanf(pos, "%s", database_name);
            char command[MAX_LENGTH + 100];
            sprintf(command, "mkdir databases/%s", database_name);
            system(command);
        }
        else
        {
            strcpy(response, "nama database kosong\n");
            return EXIT_FAILURE;
        }
    }

    FILE *file = fopen("databases/databaseusers/users.csv", "r");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka users.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file);

    char command[BUFFER_SIZE];

    fprintf(file, "\n");

    while (fgets(line, sizeof(line), file) != NULL)
    {
        char *username = strtok(line, ",");
        if (username != NULL)
        {
            if (strcmp(username, userClient) == 0)
            {
                printf("username: %s\n", username);
                sprintf(command, "echo '1,%s,%s' >> 'databases/databaseusers/permissions.csv'", database_name, username);
                system(command);
            }
            else
            {
                printf("username: %s\n", username);
                sprintf(command, "echo '0,%s,%s' >> 'databases/databaseusers/permissions.csv'", database_name, username);
                system(command);
            }
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}


int createTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[100];
    sscanf(request, "CREATE TABLE %s", tableName);

    // Membuat nama file dengan format <tableName>.csv
    // char fileName[100];
    char fileName[MAX_LENGTH + 100];
    char command[MAX_LENGTH + 100];
    sprintf(command, "touch databases/%s/%s.csv", databaseUsed, tableName);
    printf("database used: %s\n", databaseUsed);
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);
    system(command);
    // sprintf(fileName, "%s.csv", tableName);

    // Membuka file untuk ditulis
    FILE *file = fopen(fileName, "w");
    if (file == NULL)
    {
        printf("Gagal membuat file CSV.\n");
        return EXIT_FAILURE;
    }

    // Menulis baris pertama dengan tipe data kolom
    char *columnStart = strchr(request, '(') + 1;
    char *columnEnd = strchr(request, ')');
    *columnEnd = '\0'; // Mengganti ')' dengan null terminator

    // Menghapus spasi setelah koma
    char *currentChar = columnStart;
    while (currentChar != columnEnd)
    {
        if (*currentChar == ',' && *(currentChar + 1) == ' ')
        {
            memmove(currentChar + 1, currentChar + 2, strlen(currentChar + 1) + 1);
        }
        currentChar++;
    }

    // fprintf(file, "%s\n", columnStart);

    // Menulis baris kedua dengan nama kolom
    char *columnName = strtok(columnStart, ",");
    while (columnName != NULL)
    {
        fprintf(file, "%s", columnName);

        columnName = strtok(NULL, ",");
        if (columnName != NULL)
        {
            fprintf(file, ",");
        }
    }
    fprintf(file, "\n");

    // Menutup file
    fclose(file);

    printf("File CSV '%s' telah berhasil dibuat.\n", fileName);

    return EXIT_SUCCESS;
}

int dropDatabase(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }
    char *delimiter = "DATABASE";
    char *database_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        database_name = pos;

        database_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (database_name != NULL)
        {
            sscanf(pos, "%s", database_name);
        }
        else
        {
            strcpy(response, "nama database kosong\n");
            return EXIT_FAILURE;
        }
    }

    if (strcmp(database_name, databaseUsed) != 0)
    {
        strcpy(response, "Database sedang tidak digunakan\n");
        return EXIT_FAILURE;
    }
    else
    {
        char command[MAX_LENGTH + 100];
        sprintf(command, "rm -rf databases/%s", database_name);
        system(command);
    }

    return EXIT_SUCCESS;
}

int dropTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }
    char *delimiter = "TABLE";
    char *table_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        table_name = pos;

        table_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (table_name != NULL)
        {
            sscanf(pos, "%s", table_name);
        }
        else
        {
            strcpy(response, "nama table kosong\n");
            return EXIT_FAILURE;
        }
    }

    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);
    remove(file_name);

    return EXIT_SUCCESS;
}

int dropColumn(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *substr1 = "COLUMN";
    char *substr2 = "FROM";

    // mencari posisi substring "PERMISSION"
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax COLUMN tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax FROM tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    // Get Database Name
    int length = pos2 - pos1 - 1;
    char column_name[length];
    strncpy(column_name, pos1, length);
    column_name[length] = '\0';

    printf("Column Name: %s\n", column_name);

    char *delimiter = "FROM";
    char *table_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        table_name = pos;

        table_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (table_name != NULL)
        {
            sscanf(pos, "%s", table_name);
        }
        else
        {
            strcpy(response, "nama table kosong\n");
            return EXIT_FAILURE;
        }
    }

    printf("Table Name: %s\n", table_name);

    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    char command[MAX_LENGTH + 1000];
    sprintf(command, "sed -i 's/^%s[^,]*,//' %s", column_name, file_name);
    system(command);

    return EXIT_SUCCESS;
}

int addUserPermission(char *request, char *response)
{
    DIR *dir;
    struct dirent *entry;

    char *substr1 = "USER";
    char *substr2 = "IDENTIFIED";

    // sscanf(request, "CREATE USER %s IDENTIFIED BY %s", user, pass);
    // mencari posisi substring "PERMISSION"
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax USER tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax IDENTIFIED tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    int length = pos2 - pos1 - 1;
    char username[length];
    strncpy(username, pos1, length);
    username[length] = '\0';
    // printf("bah : %s", username);

    // membuka direktori "databases"

    dir = opendir("databases");
    if (dir == NULL)
    {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    // membuka permissions.csv
    FILE *file = fopen("databases/databaseusers/permissions.csv", "a");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka permissions.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }
    fseek(file, 0, SEEK_END);
    if (ftell(file) > 0)
    {
        fprintf(file, "\n");
    }
    // membaca isi direktori "databases"
    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_DIR)
        {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
            {
                fprintf(file, "%s,%s,%s\n", "0", entry->d_name, username);
                printf("%s %s\n", entry->d_name, username);
            }
        }
    }

    fclose(file);
    closedir(dir);

    return 1;
}

int grantPermission(char *request, char *response)
{

    DIR *dir;
    struct dirent *entry;

    char *substr1 = "PERMISSION";
    char *substr2 = "INTO";

    // mencari posisi substring "PERMISSION"
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax PERMISSION tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax INTO tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    // Get Database Name
    int length = pos2 - pos1 - 1;
    char database_name[length];
    strncpy(database_name, pos1, length);
    database_name[length] = '\0';

    printf("Database Name: %s\n", database_name);

    // Get User Name
    char *username = NULL;

    char *pos = strstr(request, substr2);
    if (pos != NULL)
    {
        pos += strlen(substr2) + 1;
        username = pos;

        username = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (username != NULL)
        {
            sscanf(pos, "%s", username);
        }
    }

    if (username != NULL)
    {
        printf("Username: %s\n", username);
    }

    FILE *file = fopen("databases/databaseusers/permissions.csv", "r");
    if (file == NULL)
    {
        strcpy(response, "Gagal membuka permissions.csv\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    int databaseFound = 0;
    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file);

    while (fgets(line, sizeof(line), file) != NULL)
    {
        char *token = strtok(line, ",");
        if (token != NULL && strcmp(token, "0") == 0)
        {
            token = strtok(NULL, ",");
            printf("Token: %s\n", token);
            if (token != NULL && strcmp(token, database_name) == 0)
            {
                token = strtok(NULL, ",");
                printf("Token: %s\n", token);
                printf("Username: %s\n", username);
                // username = strcat(username, "\n");
                if (token != NULL && strcmp(token, username) == 0 || strcspn(username, "\n") == strlen(username))
                {
                    databaseFound = 1;
                    break;
                }
            }
        }
    }

    fclose(file);

    if (databaseFound)
    {
        printf("Database found\n");
        char command[MAX_LENGTH + 200];
        sprintf(command, "awk -F',' -v db='%s' -v usr='%s' 'BEGIN {OFS=\",\"} NR>1 && $2==db && $3==usr {sub(/0/, \"1\", $1)} { print }' databases/databaseusers/permissions.csv > databases/databaseusers/temp.csv && mv databases/databaseusers/temp.csv databases/databaseusers/permissions.csv", database_name, username);
        int status = system(command);
        if (status == 0)
        {
            printf("Permission granted\n");
            strcpy(response, "Permission granted\n");
        }
        else
        {
            printf("Permission not granted\n");
            strcpy(response, "Permission not granted\n");
        }
    }
    else
    {
        printf("Database not found\n");
        strcpy(response, "Database tidak ditemukan\n");
        return EXIT_FAILURE;
    }

    return 1;
}

// DML DATA MANIPULATION LANGUAGE
int insertIntoTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *substr1 = "INTO";
    char *substr2 = "VALUES";

    // Find the position of "INTO" substring
    char *pos1 = strstr(request, substr1);
    if (pos1 == NULL)
    {
        strcpy(response, "Syntax INTO not found\n");
        return EXIT_FAILURE;
    }

    pos1 += strlen(substr1) + 1;

    // Find the position of "VALUES" substring
    char *pos2 = strstr(pos1, substr2);
    if (pos2 == NULL)
    {
        strcpy(response, "Syntax VALUES not found\n");
        return EXIT_FAILURE;
    }

    // Get the table name
    int length = pos2 - pos1 - 1;
    char table_name[length];
    strncpy(table_name, pos1, length);
    table_name[length] = '\0';

    printf("Table Name: %s\n", table_name);

    // Get the values to be inserted
    pos2 += strlen(substr2) + 1;
    char values[MAX_LENGTH];
    strncpy(values, pos2, MAX_LENGTH);

    // Remove leading and trailing whitespace characters
    char trimmed_values[MAX_LENGTH];
    int i = 0, j = 0;
    while (values[i] != '\0')
    {
        if (values[i] != ' ' && values[i] != '(' && values[i] != ')')
        {
            trimmed_values[j++] = values[i];
        }
        i++;
    }
    trimmed_values[j] = '\0';

    printf("Values: %s\n", trimmed_values);

    // Create the file name
    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    // Open the file in append mode
    FILE *file = fopen(file_name, "a");
    if (file == NULL)
    {
        strcpy(response, "Failed to open CSV file\n");
        perror("fopen");
        return EXIT_FAILURE;
    }

    // Write the values to a new line in the CSV file
    fprintf(file, "%s\n", trimmed_values);

    // Close the file
    fclose(file);

    return EXIT_SUCCESS;
}

int selectAllFromTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[100];
    sscanf(request, "SELECT * FROM %s", tableName);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    // Open the file for reading
    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        sprintf(response, "Table '%s' does not exist\n", tableName);
        return EXIT_FAILURE;
    }

    // Read and print each line in the file
    char line[MAX_LENGTH];
    strcat(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        strcat(response, line);
    }

    // Close the file
    fclose(file);

    return EXIT_SUCCESS;
}

int sellect_all_from_table_where(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    char condition[MAX_LENGTH];
    sscanf(request, "SELECT * FROM %s WHERE %s = %[^\n]", tableName, columnName, condition);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column names to the response
    strcpy(response, line);
    strcpy(response, "\n");
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    strcat(response, line); // Append the original line to the response
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}

int select_column_from_table(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    sscanf(request, "SELECT %s FROM %s", columnName, tableName);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Append the column value to the response
                strcat(response, columnValue);
                strcat(response, "\n");
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}

int select_column_from_table_where(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    char condition[MAX_LENGTH];
    sscanf(request, "SELECT %s FROM %s WHERE %s", columnName, tableName, condition);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)

    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    // Copy the column name to the response
    strcpy(response, columnName);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column value from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                // Check if the condition is met
                if (strcmp(columnValue, condition) == 0)
                {
                    // Append the column value to the response
                    strcat(response, columnValue);
                    strcat(response, "\n");
                }
                break;
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }
    }

    fclose(file);

    return EXIT_SUCCESS;
}


int deleteFromTable(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char *delimiter = "FROM";
    char *table_name = NULL;

    char *pos = strstr(request, delimiter);
    if (pos != NULL)
    {
        pos += strlen(delimiter) + 1;
        table_name = pos;

        table_name = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (table_name != NULL)
        {
            sscanf(pos, "%s", table_name);
        }
        else
        {
            strcpy(response, "Table name is empty\n");
            return EXIT_FAILURE;
        }
    }

    char file_name[MAX_LENGTH + 100];
    sprintf(file_name, "databases/%s/%s.csv", databaseUsed, table_name);

    char command[MAX_LENGTH + 1000];
    sprintf(command, "sed -i '2,$d' %s", file_name);
    system(command);

    return EXIT_SUCCESS;
}

int delete_from_table_where(char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    char tableName[MAX_LENGTH];
    char columnName[MAX_LENGTH];
    char condition[MAX_LENGTH];
    sscanf(request, "DELETE FROM %s WHERE %s = %[^\n]", tableName, columnName, condition);

    char fileName[MAX_LENGTH + 100];
    sprintf(fileName, "databases/%s/%s.csv", databaseUsed, tableName);

    FILE *file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("Failed to open CSV file.\n");
        return EXIT_FAILURE;
    }

    char tempFileName[MAX_LENGTH + 100];
    sprintf(tempFileName, "databases/%s/temp.csv", databaseUsed);

    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL)
    {
        printf("Failed to create temporary CSV file.\n");
        fclose(file);
        return EXIT_FAILURE;
    }

    char line[MAX_LENGTH];
    fgets(line, sizeof(line), file); // Read the first line (column names and data types)
    fputs(line, tempFile);
    // Check if the requested column exists in the table
    int columnExists = 0;
    char *token = strtok(line, ",");
    int columnIndex = 0;
    while (token != NULL)
    {
        if (strstr(token, columnName) != NULL)
        {
            columnExists = 1;
            break;
        }
        token = strtok(NULL, ",");
        columnIndex++;
    }

    if (!columnExists)
    {
        strcpy(response, "Requested column does not exist in the table.\n");
        fclose(file);
        fclose(tempFile);
        return EXIT_FAILURE;
    }

    // Copy the column names to the response
    strcpy(response, line);
    strcat(response, "\n");

    // Iterate through each line of the file
    while (fgets(line, sizeof(line), file) != NULL)
    {
        // Create a temporary copy of the line
        char tempLine[MAX_LENGTH];
        strcpy(tempLine, line);

        // Extract the column values from the line
        char *columnValue = strtok(tempLine, ",");
        int columnCount = 0;
        int matchFound = 0;

        // Iterate through each column value
        while (columnValue != NULL)
        {
            if (columnCount == columnIndex)
            {
                // Skip the leading whitespace
                while (isspace(*columnValue))
                    columnValue++;

                if (strcmp(columnValue, condition) == 0) // Check if the condition is met
                {
                    matchFound = 1;
                    break;
                }
            }

            columnValue = strtok(NULL, ",");
            columnCount++;
        }

        // If the condition is not met, write the line to the temporary file
        if (!matchFound)
            fputs(line, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove(fileName);
    rename(tempFileName, fileName);

    return EXIT_SUCCESS;
}

int updateData(const char *request, char *response)
{
    if (databaseUsed == NULL)
    {
        strcpy(response, "No database used\n");
        return EXIT_FAILURE;
    }

    // Extract table name, set column, set new value, where column, and where value from the request
    char table[100];
    char setColumn[100];
    char setNewValue[100];
    char whereColumn[100];
    char whereValue[100];

    if (sscanf(request, "UPDATE %s SET %s = %s WHERE %s = %s", table, setColumn, setNewValue, whereColumn, whereValue) != 5)
    {
        strcpy(response, "Invalid UPDATE command\n");
        return EXIT_FAILURE;
    }

    // Construct the file path
    char filePath[200];
    sprintf(filePath, "databases/%s/%s.csv", databaseUsed, table);

    // Open the CSV file for reading
    FILE *file = fopen(filePath, "r");
    if (file == NULL)
    {
        sprintf(response, "Failed to open the file '%s'\n", filePath);
        return EXIT_FAILURE;
    }

    // Create a temporary file for writing the updated data
    FILE *tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL)
    {
        fclose(file);
        sprintf(response, "Failed to create the temporary file\n");
        return EXIT_FAILURE;
    }

    // Read and process the header line
    char headerLine[1024];
    fgets(headerLine, sizeof(headerLine), file);
    fputs(headerLine, tempFile);

    // Read and process each data line
    char dataLine[1024];
    int updated = 0;

    while (fgets(dataLine, sizeof(dataLine), file) != NULL)
    {
        // Remove any existing newline characters from the data line
        char *newline = strchr(dataLine, '\n');
        if (newline != NULL)
        {
            *newline = '\0';
        }

        // Extract the column values from the data line
        char *column = strtok(dataLine, ",");
        char currentColumn[100];
        int columnIndex = 0;

        while (column != NULL)
        {
            strcpy(currentColumn, column);

            if (columnIndex == 0 && strcmp(currentColumn, whereValue) == 0 && strcmp(setColumn, whereColumn) == 0)
            {
                // Update the column value
                strcpy(currentColumn, setNewValue);
                updated = 1;
            }

            fprintf(tempFile, "%s", currentColumn);

            column = strtok(NULL, ",");
            columnIndex++;

            if (column != NULL)
            {
                fprintf(tempFile, ",");
            }
        }

        // Write a new line
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (updated)
    {
        // Replace the original file with the temporary file
        if (remove(filePath) != 0)
        {
            sprintf(response, "Failed to remove the original file\n");
            return EXIT_FAILURE;
        }

        if (rename("temp.csv", filePath) != 0)
        {
            sprintf(response, "Failed to rename the temporary file\n");
            return EXIT_FAILURE;
        }

        sprintf(response, "Data updated successfully.\n");
    }
    else
    {
        // Remove the temporary file if no updates were made
        remove("temp.csv");
        sprintf(response, "No matching records found for the given conditions.\n");
    }

    return EXIT_SUCCESS;
}

int backupData(char *request, char *response){

    char namaDB[500];

    sscanf(request, "BACKUP DATABASE %s", namaDB);

    char directoryDB[500];
    sprintf(directoryDB, "databases/%s", namaDB);
    DIR* dir = opendir(directoryDB);

    if (dir) {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) {
            entry->d_name[strlen(entry->d_name) -4] = '\0';

            // Exclude current directory (".") and parent directory ("..")
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                sprintf(response, "DROP TABLE %s", entry->d_name);
                send(new_socket, response, BUFFER_SIZE, 0);

                char dirfile[200];
                sprintf(dirfile, "%s/%s.csv", directoryDB, entry->d_name);
                FILE* file = fopen(dirfile, "r");
                if (file == NULL) {
                    printf("Failed to open the file.\n");
                    return 1;
                }

                char line[BUFFER_SIZE];
                int idx = 1;
                while (fgets(line, sizeof(line), file) != NULL) {
                    // Process the line
                    if(idx == 1){
                        line[strlen(line) -1] = '\0';
                        sprintf(response, "CREATE TABLE (%s);", line);
                        send(new_socket, response, BUFFER_SIZE, 0);
                    }else{
                        line[strlen(line) -1] = '\0';
                        sprintf(response, "INSERT INTO %s VALUES (%s);", entry->d_name, line);
                        send(new_socket, response, BUFFER_SIZE, 0);
                    }

                    idx++;
                }

                fclose(file);
                send(new_socket, "\n", BUFFER_SIZE, 0);
            }

        }
        closedir(dir);

        sprintf(response, "//DONE BACKUP");
    } else {
        sprintf(response, "Database not exist\n");
        return EXIT_FAILURE;
    }

}

int execute(char *request, char *response, char *user_client)
{
    generateDatabaseStateByUserClient();

    printf("%s\n", request);
    request[strcspn(request, "\n")] = 0;

    if (strstr(request, "GRANT PERMISSION") && isRoot)
    {
        strcpy(response, "GRANT PERMISSION");
        grantPermission(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "CREATE USER"))
    {
        if (isRoot)
        {
            strcpy(response, "CREATE USER");
            printf("Masuk sini\n");
            if (strlen(request) > 12)
                createUser(request, response);
            create_log(user_client, request);
            addUserPermission(request, response);
        }
        strcpy(response, "ONLY ROOT CAN CREATE USER\n");
    }
    else if (strstr(request, "USE"))
    {
        strcpy(response, "USE");
        useDatabase(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "CREATE DATABASE"))
    {
        strcpy(response, "CREATE DATABASE");
        createDatabase(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "CREATE TABLE"))
    {
        strcpy(response, "CREATE TABLE");
        createTable(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "DROP DATABASE"))
    {
        strcpy(response, "DROP DATABASE");
        dropDatabase(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "DROP TABLE"))
    {
        strcpy(response, "DROP TABLE");
        dropTable(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "DROP COLUMN"))
    {
        strcpy(response, "DROP COLUMN");
        dropColumn(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "INSERT INTO"))
    {
        strcpy(response, "INSERT INTO");
        insertIntoTable(request, response);
        create_log(user_client, request);
    }
    else if (strstr(request, "SELECT"))
    {   
        if (strstr(request, "WHERE"))
        {
            strcpy(response, "SELECT WHERE");
            sellect_all_from_table_where(request, response);
            create_log(user_client, request);
        }
        else if (strstr(request, "*"))
        {
            strcpy(response, "SELECT ALL");
            selectAllFromTable(request, response);
            create_log(user_client, request);
        }
        else
        {
        strcpy(response, "SELECT COLUMN");
        select_column_from_table(request, response);
        create_log(user_client, request);
        }
    }
    else if (strstr(request, "DELETE"))
    {
        if (strstr(request, "WHERE"))
        {
            strcpy(response, "DELETE WHERE");
            delete_from_table_where(request, response);
            create_log(user_client, request);
        }
        else if (request, "")
        {
            strcpy(response, "DELETE FROM");
            deleteFromTable(request, response);
            create_log(user_client, request);
        }
    }

    else if (strstr(request, "UPDATE"))
    {
        strcpy(response, "UPDATE");
        updateData(request, response);
        create_log(user_client, request);
    }

    else if (strstr(request, "BACKUP DATABASE"))
    {
        strcpy(response, "BACKUP DATABASE");
        backupData(request, response);
        create_log(user_client, request);
    }

    else
    {
        strcpy(response, "ERROR");
    }

    return EXIT_SUCCESS;
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    int server_fd, valread, opt = 1;
    char tempUser[50], tempPass[50], command[1000];

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        char request[BUFFER_SIZE], response[BUFFER_SIZE];

        if (recv(new_socket, request, BUFFER_SIZE, 0) == 0)
        {
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            else
                recv(new_socket, request, BUFFER_SIZE, 0);
        }

        if (strcmp(request, "root") == 0)
        {
            strcpy(response, "true");
            send(new_socket, response, BUFFER_SIZE, 0);
            isRoot = true;
            strcpy(userClient, "root");
            continue;
        }
        else if (sscanf(request, "%s%s", tempUser, tempPass) == 2 || strcmp(tempUser, "USE") != 0)
        {
            // printf("temp : %s %s\n", tempUser, tempPass);
            snprintf(command, sizeof(command), "awk -F',' '$1 == \"%s\" && $2 == \"%s\"' databases/databaseusers/users.csv", tempUser, tempPass);

            FILE *pipe = popen(command, "r");
            if (pipe == NULL)
            {
                printf("Gagal membuka pipa perintah.\n");
                return 1;
            }
            // printf("%s %s\n", userClient, passClient);
            char username[50], password[50], result[50];

            while (fgets(result, sizeof(result), pipe) != NULL)
            {
                sscanf(result, "%[^,],%s", username, password);
            }

            pclose(pipe);

            if (strcmp(tempUser, username) == 0 && strcmp(tempPass, password) == 0)
            {
                isRoot = false;
                strcpy(userClient, tempUser);
                strcpy(passClient, tempPass);
                strcpy(response, "true");
                printf("%s\n", response);
                send(new_socket, response, BUFFER_SIZE, 0);
                continue;
            }
        }
        execute(request, response, userClient);
        send(new_socket, response, BUFFER_SIZE, 0);
        memset(request, '\0', sizeof(request));
        printf("user : %s\n", userClient);
    }

    return 0;
}
