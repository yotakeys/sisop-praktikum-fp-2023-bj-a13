#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

#define BUFFER_SIZE 1024

#define ADDRESS "127.0.0.1"
#define PORT 8080

char name_file[100];
bool isLogin = false;

bool isRoot()
{
    return (getuid() == 0);
}

bool authenticate(int argc, char const *argv[], int *sock)
{
    char buffer[1024] = {0}, response[20];
    int valread;
    // printf("Masuk\n");
    if (!isRoot())
    {
        if ((strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0))
        {
            send(*sock, "error", strlen("error"), 0);
            printf("Authentication error!\n");
            printf("Syntax to start the client:\n");
            printf("./program -u [username] -p [password]\n");
            return false;
        }
        // printf("MASOKKK\n");
        // Set first msg for authentication
        strcpy(buffer, argv[2]);
        strcat(buffer, " ");
        strcat(buffer, argv[4]);
        printf("%s\n", buffer);

        send(*sock, buffer, BUFFER_SIZE, 0);
    }
    else
    {
        if (argc == 1)
        {
            strcpy(buffer, "root");
            send(*sock, buffer, strlen(buffer), 0);
        }
    }
    recv(*sock, response, BUFFER_SIZE, 0);
    printf("res : %s\n", response);

    if (strcmp(response, "true") == 0)
    {
        isLogin = true;
    }

    memset(buffer, '\0', sizeof(buffer));
    return true;
}

void *messageHandling(void *arg)
{
    char buffer[1024];

    memset(buffer, 0, sizeof(buffer));
    while (read(*(int *)arg, buffer, 1024) != 0)
    {
        if (strlen(buffer)){
            char cmd[100];
            sprintf(cmd, "echo \"%s\" >> %s", buffer, name_file);
            system(cmd);
        }

        memset(buffer, 0, sizeof(buffer));
    }
}


int main(int argc, char const *argv[])
{
    int sock = 0, valread;
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address or address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    if (!authenticate(argc, argv, &sock))
    {
        return 0;
    }

    if (!isLogin)
    {
        return 0;
    }


    char buffer[BUFFER_SIZE];
    
    strcpy(buffer, "BACKUP DATABASE ");
    strcat(buffer, argv[5]);

    strcpy(name_file, argv[7]);

    send(sock, buffer, strlen(buffer), 0);

    pthread_t dataHandler;
    pthread_create(&dataHandler, NULL, &messageHandling, &sock);


    memset(buffer, 0, sizeof(buffer));

    // Join thread
    pthread_join(dataHandler, NULL);

    close(sock);

    return 0;
}
